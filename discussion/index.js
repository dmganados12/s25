/*Create a standard server app using Node.JS. 

 - Get the http module using the require() directive.
 - Repackage the module on a new variable.
 */

 	const http = require('http');
 	const host = 4000;

 // Start the server

 // Create the server and place it inside a new variable to give it an identifier.
 	let server = http.createServer( (req, res) => {/*we used the ES6 format. And arguments (req and res) that used to 'catch' data and pass it along inside our function.*/
        res.end('Welcome to the App');
 	})

 // Assigne a designated port that will serve the project, by binding the connection with the desired port

 	server.listen(host);

// include a response that will be displayed in the console to verify the connection that was established.
    console.log(`Listening on port: ${host}`)